import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class RotatingWidget extends HookWidget {
  final Duration duration;
  final Widget child;

  const RotatingWidget({
    super.key,
    required this.child,
    this.duration = const Duration(seconds: 2),
  });

  @override
  Widget build(BuildContext context) {
    final controller =
    useAnimationController(duration: const Duration(milliseconds: 1500));
    final rotation = useState<double>(0);
    final tween = Tween<double>(begin: 0, end: 2 * pi).animate(controller);
    tween.addListener(() => rotation.value = tween.value);

    useEffect(() {
      controller.repeat();

      return () => controller.stop();
    }, []);

    return SizedBox.expand(
        child:
        Transform.rotate(
          angle: rotation.value,
          child: Center(
            child: child,
          ),
        )
    );
  }
}

//Icons.light_mode
//Icons.camera
//Icons.catching_pokemon
//Icons.explore_outlined
//Icons.filter_vintage
//Icons.scatter_plot
//Icons.sports_baseball_outlined
//Icons.sports_soccer
//Icons.sports_volleyball_outlined
//Icons.stars_rounded
//Icons.support
//Icons.remove_circle_outline,
//Icons.filter_tilt_shift
//Icons.settings
