import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:intl/intl.dart';

class TimeWidget extends HookWidget {
  final DateTime date;
  final void Function(DateTime) onChanged;

  TimeWidget({required this.date, required this.onChanged});

  String getFormattedDate(DateTime date) {
    return DateFormat('HH:mm').format(date);
  }

  @override
  Widget build(BuildContext context) {
    final controller = useTextEditingController(text: getFormattedDate(date));

    useEffect(() {
      controller.text = getFormattedDate(date);
      print(date);

      return;
    }, [date]);

    return TextField(
      readOnly: true,
      controller: controller,
      decoration: const InputDecoration(hintText: 'Pick your Date'),
      onTap: () async {
        var time = await showTimePicker(
          context: context,
          initialTime: TimeOfDay.fromDateTime(date),
        );
        if (time != null) {
          final newDate = date.copyWith(hour: time.hour, minute: time.minute);
          onChanged(newDate);
          controller.text = getFormattedDate(newDate);
        }
      },
    );
  }
}
