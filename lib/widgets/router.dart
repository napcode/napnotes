import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'dart:async' show StreamSubscription;
import 'package:napnotes/core/event_bus.dart';
import 'package:napnotes/core/events.dart';
import 'package:napnotes/core/enums.dart';
import 'package:napnotes/core/app_state.dart';
import 'package:napnotes/views/note_detail_view.dart';
import 'package:napnotes/views/settings_view.dart';
import 'package:napnotes/views/notes_view.dart';
import 'package:napnotes/views/events_view.dart';
import 'package:napnotes/views/event_detail_view.dart';

class MyRouter extends HookWidget {
  const MyRouter();

  Widget getView(AppView view, dynamic data) {
    switch (view) {
      case AppView.notes:
        return NotesView();
      case AppView.events:
        return EventsView();
      case AppView.settings:
        return SettingsView();
      case AppView.noteDetail:
        return NoteDetailView(id: data);
      case AppView.addNote:
        return NoteDetailView(id: null);
      case AppView.eventDetail:
        return EventDetailView(id: data);
      case AppView.addEvent:
        return EventDetailView(id: null);
    }
  }

  @override
  Widget build(BuildContext context) {
    final currentView = useState(AppView.events);
    final viewData = useState<dynamic>(null);

    void changeRoute({required AppView view, dynamic data}) {
      currentView.value = view;
      viewData.value = data;
      AppState.setCurrentView(view);
    }

    useEffect(() {
      StreamSubscription? eventBusSubscription = EventBus.stream.listen(
        (event) {
          if (event.runtimeType == BottomAppBarButtonPressed) {
            changeRoute(view: event.view);
          }
          if ([NoteClicked, NoteSaved].contains(event.runtimeType)) {
            changeRoute(view: AppView.noteDetail, data: event.id);
          }
          if (event.runtimeType == NoteDeleted) {
            changeRoute(view: AppView.notes);
          }
          if (event.runtimeType == EventDeleted) {
            changeRoute(view: AppView.events);
          }
          if ([EventClicked].contains(event.runtimeType)) {
            changeRoute(view: AppView.eventDetail, data: event.id);
          }
          if (event.runtimeType == BackButtonClicked) {
            if ([AppView.eventDetail, AppView.addEvent]
                .contains(AppState.currentView)) {
              changeRoute(view: AppView.events);
            } else if ([AppView.noteDetail, AppView.addNote]
                .contains(AppState.currentView)) {
              changeRoute(view: AppView.notes);
            } else {
              changeRoute(view: AppView.events);
            }
          }
          if (event.runtimeType == RouteChanged) {
            AppState.update();
          }
        },
      );

      return () => eventBusSubscription.cancel();
    }, []);

    return Center(child: getView(currentView.value, viewData.value));
  }
}
