import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import 'package:napnotes/core/dataclasses.dart';
import 'package:napnotes/core/event_bus.dart';
import 'package:napnotes/core/events.dart';
import 'package:napnotes/styles.dart';

class DayCard extends HookWidget {
  final Day day;

  const DayCard({required this.day});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(left: 16, right: 16, top: 16),
      elevation: 4,
      clipBehavior: Clip.hardEdge,
      child: Column(
        children: [
          _DayHeader(day: day),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: day.events.isNotEmpty
                ? _getEventsList(day.events)
                : _getEmptyDayPlaceholder(),
          ),
        ],
      ),
    );
  }
}

Widget _getEventsList(events) {
  return Column(children: [
    for (final event in events)
      event == events.last
          ? _Event(event: event)
          : Column(
              children: [
                _Event(event: event),
                const Padding(
                  padding:
                      EdgeInsets.only(top: 12, bottom: 12, left: 16, right: 16),
                  child: SizedBox(
                    height: 2,
                    width: double.infinity,
                    child: ColoredBox(color: primaryColor),
                  ),
                ),
              ],
            ),
  ]);
}

Widget _getEmptyDayPlaceholder() {
  return const Column(children: [
    Text('There are no events.', style: contentTextStyle),
    Icon(Icons.favorite, color: secondaryColor)
  ]);
}

class _DayHeader extends StatelessWidget {
  const _DayHeader({super.key, required this.day});

  final Day day;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: day.daysSinceToday == 0 ? secondaryColor : primaryColor),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Text(
                day.formattedDate,
                style: calendarTitleTextStyle,
              ),
            ),
            Flexible(
              child: Text(
                day.daysSinceToday == 0 ? 'TODAY' : day.daysSinceTodayString,
                style: calendarTitleTextStyle,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _Event extends StatelessWidget {
  final CalendarEvent event;

  const _Event({super.key, required this.event});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        EventBus.send(EventClicked(event.id));
      },
      behavior: HitTestBehavior.translucent,
      child: Row(
        children: [
          Expanded(
            flex: 6,
            child: Center(
              child: Text(
                event.title,
                style: contentTextStyle,
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(Icons.schedule, size: 24),
                  const Text(' '),
                  Text(
                    event.formattedTime,
                    style: contentTextStyle,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
