import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:napnotes/core/event_bus.dart';
import 'package:napnotes/core/events.dart';
import 'package:napnotes/core/enums.dart';
import 'package:napnotes/styles.dart';
import 'package:napnotes/core/app_state.dart';

class MyBottomAppBar extends HookWidget {
  MyBottomAppBar();

  @override
  Widget build(BuildContext context) {
    useAppState(context);

    return SizedBox(
      height: BOTTOM_BAR_HEIGHT_WITH_OPTIONS,
      child: Stack(
        children: [
          if (AppState.currentView == AppView.notes ||
              AppState.currentView == AppView.events)
            _OptionsBar(),
          Align(
            alignment: Alignment.bottomLeft,
            child: Stack(
              children: [
                Positioned.fill(
                    child: CustomPaint(painter: _BackgroundPainter())),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    _Button(
                        iconData: Icons.sticky_note_2_outlined,
                        isActive: AppState.currentView == AppView.notes,
                        onClick: () {
                          EventBus.send(
                              BottomAppBarButtonPressed(view: AppView.notes));
                        }),
                    _Button(
                        iconData: Icons.calendar_month,
                        isActive: AppState.currentView == AppView.events,
                        onClick: () {
                          EventBus.send(
                              BottomAppBarButtonPressed(view: AppView.events));
                        }),
                    _Button(
                        iconData: Icons.settings,
                        isActive: AppState.currentView == AppView.settings,
                        onClick: () {
                          EventBus.send(BottomAppBarButtonPressed(
                              view: AppView.settings));
                        }),
                    _Button(
                        iconData: Icons.add,
                        isActive: true,
                        size: _BIG_ICON_SIZE,
                        onClick: () {
                          var view = AppView.addNote;
                          if ([
                            AppView.events,
                            AppView.eventDetail,
                            AppView.addEvent
                          ].contains(AppState.currentView)) {
                            view = AppView.addEvent;
                          }
                          EventBus.send(
                            BottomAppBarButtonPressed(view: view),
                          );
                        }),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _Button extends HookWidget {
  _Button({
    required this.iconData,
    required this.isActive,
    this.size = _SMALL_ICON_SIZE,
    required this.onClick,
    this.iconColor = Colors.white,
  });

  final IconData iconData;
  final bool isActive;
  final double size;
  final VoidCallback onClick;
  final Color iconColor;

  @override
  Widget build(BuildContext context) {
    final isDown = useState(false);
    final isHover = useState(false);

    return InkResponse(
      highlightShape: BoxShape.circle,
      hoverColor: Colors.transparent,
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      onTap: onClick,
      onTapDown: (_) {
        isDown.value = true;
      },
      onTapUp: (_) {
        isDown.value = false;
      },
      onTapCancel: () {
        isDown.value = false;
      },
      onHover: (currentHover) {
        isHover.value = currentHover;
      },
      child: Stack(
        children: [
          Positioned.fill(
              child: AnimatedScale(
            duration: const Duration(milliseconds: 300),
            scale: isHover.value ? 1.0 : 0.0,
            child: const ClipOval(
                child: ColoredBox(
              color: Colors.white10,
            )),
          )),
          Positioned.fill(
              child: AnimatedScale(
            duration: const Duration(milliseconds: 300),
            scale: isDown.value ? 1.0 : 0.0,
            child: const ClipOval(
                child: ColoredBox(
              color: Colors.white30,
            )),
          )),
          Padding(
              padding: const EdgeInsets.all(16),
              child: AnimatedOpacity(
                duration: const Duration(milliseconds: 300),
                opacity: isActive ? 1 : 0.6,
                child: Icon(
                  iconData,
                  size: size,
                  color: iconColor,
                ),
              )),
        ],
      ),
    );
  }
}

class _OptionsBar extends HookWidget {
  final searchBorder = OutlineInputBorder(
    borderSide: const BorderSide(
      width: 3,
      color: primaryColor,
    ),
    borderRadius: BorderRadius.circular(50.0),
  );

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final searchPhrase = useState('');
    final controller = useTextEditingController();

    useEffect(() {
      final timer = Timer(const Duration(milliseconds: 500), () {
        EventBus.send(SearchPhraseChanged(searchPhrase.value));
      });

      return timer.cancel;
    }, [searchPhrase.value]);

    useEffect(() {
      searchPhrase.value = '';
      controller.text = '';
    }, [AppState.currentView]);

    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: controller,
              decoration: InputDecoration(
                prefixIcon: const Icon(Icons.search),
                hintText: 'search...',
                fillColor: Colors.white,
                filled: true,
                enabledBorder: searchBorder,
                focusedBorder: searchBorder,
                suffixIcon: searchPhrase.value.isNotEmpty
                    ? GestureDetector(
                        onTap: () {
                          searchPhrase.value = '';
                          controller.text = '';
                        },
                        child: const Icon(
                          Icons.close,
                          color: primaryColor,
                        ),
                      )
                    : const SizedBox(),
              ),
              onChanged: (search) {
                searchPhrase.value = search.toLowerCase();
              },
            ),
          ),
        ),
        if (AppState.currentView == AppView.notes)
          _OptionButton(
              iconData: Icons.favorite,
              isActive: AppState.currentView == AppView.notes,
              iconColor: primaryColor,
              onClick: () {
                EventBus.send(FilterFavoriteClicked());
              }),
        if (AppState.currentView == AppView.events)
          _OptionButton(
              iconData: Icons.all_inclusive,
              isActive: AppState.currentView == AppView.notes,
              iconColor: primaryColor,
              onClick: () {
                EventBus.send(ShowAllEventsClicked());
              }),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            width: screenWidth / 4,
          ),
        ) //TODO FIX
      ],
    );
  }
}

class _OptionButton extends HookWidget {
  _OptionButton({
    required this.iconData,
    required this.isActive,
    required this.onClick,
    this.iconColor = Colors.white,
  });

  final IconData iconData;
  final bool isActive;
  final VoidCallback onClick;
  final Color iconColor;

  @override
  Widget build(BuildContext context) {
    final isDown = useState(false);
    final isHover = useState(false);

    return InkResponse(
      highlightShape: BoxShape.circle,
      hoverColor: Colors.transparent,
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      onTap: onClick,
      onTapDown: (_) {
        isDown.value = true;
      },
      onTapUp: (_) {
        isDown.value = false;
      },
      onTapCancel: () {
        isDown.value = false;
      },
      onHover: (currentHover) {
        isHover.value = currentHover;
      },
      child: Stack(
        children: [
          const Positioned.fill(
            child: ClipOval(
                child: ColoredBox(
              color: Colors.white,
            )),
          ),
          Positioned.fill(
              child: AnimatedScale(
            duration: const Duration(milliseconds: 300),
            scale: isHover.value ? 1.0 : 0.0,
            child: const ClipOval(
                child: ColoredBox(
              color: Color(0x55CCCCCC),
            )),
          )),
          Positioned.fill(
              child: AnimatedScale(
            duration: const Duration(milliseconds: 300),
            scale: isDown.value ? 1.0 : 0.0,
            child: const ClipOval(
                child: ColoredBox(
              color: Colors.white30,
            )),
          )),
          Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(50)),
              border: Border.all(width: 4, color: primaryColor),
            ),
            child: Padding(
                padding: const EdgeInsets.all(8),
                child: AnimatedOpacity(
                  duration: const Duration(milliseconds: 300),
                  opacity: isActive ? 1 : 0.6,
                  child: Icon(
                    iconData,
                    size: 30,
                    color: iconColor,
                  ),
                )),
          ),
        ],
      ),
    );
  }
}

class _BackgroundPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = primaryColor
      ..style = PaintingStyle.fill;
    canvas.drawRect(
        Rect.fromLTWH(
            0, size.height - BOTTOM_BAR_HEIGHT, size.width, size.height),
        paint);
    const padding = 8;
    final widthOfSingleElement = size.width / 4;
    final radius = min(widthOfSingleElement / 2, size.height * 1.2) * 7 / 8;
    const double strokeWidth = 6;
    final whitePaint = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth;
    final secondaryPaint = Paint()
      ..color = secondaryColor
      ..style = PaintingStyle.fill;
    final circleCenter = Offset(
        size.width - widthOfSingleElement / 2 - padding, size.height / 2);
    canvas.drawCircle(circleCenter, radius, secondaryPaint);
    canvas.drawArc(
      Rect.fromCenter(
        center: circleCenter,
        width: radius * 2 + strokeWidth,
        height: radius * 2 + strokeWidth,
      ),
      -getAngleToTopEdgeOfBottomBar(radius, size.height),
      pi + getAngleToTopEdgeOfBottomBar(radius, size.height) * 2,
      false,
      whitePaint,
    );
  }

  double getAngleToTopEdgeOfBottomBar(double radius, double height) {
    const height = BOTTOM_BAR_HEIGHT / 4;
    final sin = height / radius;
    return asin(sin) + 0.1;
  }

  @override
  bool shouldRepaint(_BackgroundPainter oldDelegate) => false;
}

const double _SMALL_ICON_SIZE = 20;
const double _BIG_ICON_SIZE = 40;
const double BOTTOM_BAR_HEIGHT = 52;
const double BOTTOM_BAR_HEIGHT_WITH_OPTIONS = BOTTOM_BAR_HEIGHT + 72;
