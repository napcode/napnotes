import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:intl/intl.dart';

class DateWidget extends HookWidget {
  final DateTime date;
  final void Function(DateTime) onChanged;

  DateWidget({required this.date, required this.onChanged});

  String getFormattedDate(DateTime date) {
    return DateFormat('dd/MM/yyyy').format(date);
  }

  @override
  Widget build(BuildContext context) {
    final controller = useTextEditingController(text: getFormattedDate(date));

    useEffect(() {
      controller.text = getFormattedDate(date);

      return;
    }, [date]);

    return TextField(
      readOnly: true,
      controller: controller,
      decoration: const InputDecoration(hintText: 'Pick your Date'),
      onTap: () async {
        var selectedDate = await showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime(1900),
          lastDate: DateTime(2100),
        );
        if (selectedDate != null) {
          onChanged(selectedDate);
          controller.text = getFormattedDate(selectedDate);
        }
      },
    );
  }
}
