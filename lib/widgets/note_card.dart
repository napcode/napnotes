import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import 'package:napnotes/core/dataclasses.dart';
import 'package:napnotes/core/event_bus.dart';
import 'package:napnotes/core/events.dart';
import 'package:napnotes/styles.dart';
import 'package:napnotes/views/note_detail_view.dart';

class NoteCard extends HookWidget {
  final Note note;

  const NoteCard({required this.note});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        EventBus.send(NoteClicked(note.id));
      },
      child: Card(
        margin: const EdgeInsets.only(left: 16, right: 16, top: 16),
        elevation: 4,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(children: <Widget>[
            if (note.title.isNotEmpty)
              Column(
                children: [
                  Text(
                    note.title,
                    style: titleTextStyle,
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  const SizedBox(
                    width: double.infinity,
                    height: 1,
                    child: ColoredBox(
                      color: Colors.black,
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                ],
              ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                getDisplayableNoteContent(note),
                style: contentTextStyle,
                textAlign: TextAlign.start,
                maxLines: 7,
              ),
            )
          ]),
        ),
      ),
    );
  }
}
