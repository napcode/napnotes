import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'dart:async' show StreamSubscription;
import 'package:napnotes/core/event_bus.dart';
import 'package:napnotes/core/events.dart';
import 'package:napnotes/styles.dart';
import 'package:napnotes/widgets/rotating_widget.dart';

class LoadingScreen extends HookWidget {
  const LoadingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final loading = useState(false);

    useEffect(() {
      StreamSubscription? eventBusSubscription = EventBus.stream.listen(
        (event) {
          if ([SaveClicked, DeleteClicked].contains(event.runtimeType)) {
            loading.value = true;
          }
          final closeLoadingEvents = [
            NoteSaved,
            NoteSaveFailed,
            NoteDeleted,
            NoteDeleteFailed,
            EventSaved,
            EventSaveFailed,
            EventDeleted,
            EventDeleteFailed,
          ];
          if (closeLoadingEvents.contains(event.runtimeType)) {
            loading.value = false;
          }
        },
      );

      return () => eventBusSubscription.cancel();
    }, []);

    return Stack(
      children: [
        AnimatedOpacity(
          opacity: loading.value ? 1 : 0,
          duration: const Duration(milliseconds: 200),
          child: IgnorePointer(
            child: SizedBox.expand(
              child: ColoredBox(
                color: Colors.grey.withOpacity(0.3),
              ),
            ),
          ),
        ),
        AnimatedScale(
          scale: loading.value ? 1 : 0,
          duration: const Duration(milliseconds: 200),
          child: const AbsorbPointer(
            child: RotatingWidget(
              child: Icon(Icons.bubble_chart, size: 50, color: secondaryColor),
            ),
          ),
        ),
      ],
    );
  }
}
