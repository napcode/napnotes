import 'package:flutter/material.dart';

const titleTextStyle = TextStyle(
  color: secondaryColor,
  fontWeight: FontWeight.bold,
  decoration: TextDecoration.none,
  fontSize: 24,
);

const contentTextStyle = TextStyle(
  decoration: TextDecoration.none,
  fontSize: 16,
);

const buttonTextStyle = TextStyle(
  decoration: TextDecoration.none,
  fontWeight: FontWeight.w500,
  fontSize: 16,
);

const calendarTitleTextStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w500,
  decoration: TextDecoration.none,
  fontSize: 18,
);

const primaryColor = Color(0xffff9800);
const secondaryColor = Color(0xff2DA9A2);
const errorColor = Color(0xfff7412d);
