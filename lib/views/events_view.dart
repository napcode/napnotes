import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import 'package:napnotes/core/app_state.dart';
import 'package:napnotes/core/event_bus.dart';
import 'package:napnotes/core/events.dart';
import 'package:napnotes/core/utils.dart';
import 'package:napnotes/core/dataclasses.dart';
import 'package:napnotes/widgets/bottom_app_bar.dart';
import 'package:napnotes/widgets/day_card.dart';

void addToday(List<Day> days) {
  final today = getDay(DateTime.now());
  final todayTimestamp = toTimestamp(today);
  final index = days.indexWhere((e) => toTimestamp(e.date) >= todayTimestamp);
  final todayDay = Day(date:today, events: []);

  if (index == -1) {
    days.add(todayDay);
  } else if (days[index].date != today) {
    days.insert(index, todayDay);
  }

}
class EventsView extends HookWidget {
  List<Day> getDays(List<CalendarEvent> events, bool showAll) {
    final days = <Day>[];
    for (final event in events
        .where((e) => showAll || toTimestamp(e.date) >= getTodayTimestamp())
        .toList()) {
      final day = getDay(event.date);
      if (days.isNotEmpty && day == days.last.date) {
        days.last.events.add(event);
      } else {
        days.add(Day(date: day, events: [event]));
      }
    }
    addToday(days);

    return days;
  }

  @override
  Widget build(BuildContext context) {
    final searchFiltered = useState('');
    final showAllEvents = useState(false);
    useAppState(context);

    useEffect(() {
      StreamSubscription? eventBusSubscription =
          EventBus.stream.listen((event) {
        if (event.runtimeType == SearchPhraseChanged) {
          searchFiltered.value = (event as SearchPhraseChanged).searchPhrase;
        }
        if (event.runtimeType == ShowAllEventsClicked) {
          showAllEvents.value = !showAllEvents.value;
        }
      });

      return () => eventBusSubscription.cancel();
    }, []);

    return ListView(
        padding: const EdgeInsets.only(bottom: BOTTOM_BAR_HEIGHT_WITH_OPTIONS),
        scrollDirection: Axis.vertical,
        children: [
          for (var d
              in getDays(AppState.events, showAllEvents.value).where((day) {
            if (searchFiltered.value.isEmpty) {
              return true;
            }

            return day.events
                .where((event) =>
                    event.title.toLowerCase().contains(searchFiltered.value) ||
                    event.content.toLowerCase().contains(searchFiltered.value))
                .isNotEmpty;
          }))
            DayCard(day: d)
        ]);
  }
}
