import 'dart:async' show StreamSubscription;
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:napnotes/core/app_state.dart';
import 'package:napnotes/core/dataclasses.dart';
import 'package:napnotes/styles.dart';
import 'package:napnotes/widgets/bottom_app_bar.dart';
import 'package:napnotes/core/event_bus.dart';
import 'package:napnotes/core/events.dart';

import 'button_panel.dart';

class NoteDetailView extends HookWidget {
  final String? id;

  NoteDetailView({required this.id});

  @override
  Widget build(BuildContext context) {
    final note = useState(AppState.getNoteById(id));
    final editing = useState(id == null);
    final isList = useState(note.value.isList);
    final list = useState<List<String>>([]);
    final listKey = useState(0);

    List<ButtonData> getButtons() {
      final buttons = <ButtonData>[];
      if (editing.value == false) {
        buttons.add(ButtonData('Edit', Icons.mode_edit, () {
          EventBus.send(EditClicked());
        }));
      } else {
        buttons.add(ButtonData('Save', Icons.save, () {
          if (isList.value) {
            note.value.content = listToContent(list.value);
          }
          EventBus.send(SaveClicked());
          AppState.saveNote(note.value);
        }));
        buttons.add(ButtonData('Cancel', Icons.cancel, () {
          note.value = AppState.getNoteById(id);
          EventBus.send(CancelClicked());
        }));
      }
      buttons.add(ButtonData('Delete', Icons.delete, () {
        showDeleteModal(context, () {
          AppState.deleteNote(note.value);
          EventBus.send(DeleteClicked());
        });
      }));

      return buttons;
    }

    useEffect(() {
      StreamSubscription? eventBusSubscription = EventBus.stream.listen(
        (event) {
          switch (event.runtimeType) {
            case EditClicked:
              editing.value = true;
              break;
            case CancelClicked:
              editing.value = false;
              break;
            case SaveClicked:
              //TODO: AppState update note
              editing.value = false;
              break;
            case ListElementAdded:
              list.value = [...list.value, (event as ListElementAdded).value];
              break;
            case ListElementChanged:
              var change = event as ListElementChanged;
              list.value[change.id] = change.value;
              list.value = [...list.value];
              if (editing.value == false) {
                note.value.content = listToContent(list.value);
                AppState.saveNote(note.value);
              }
              break;
            case ListElementRemoved:
              var id = (event as ListElementRemoved).id;
              var secondPartOfList = [];
              if (id < list.value.length - 1) {
                secondPartOfList = [...list.value.sublist(id + 1)];
              }
              list.value = [...list.value.sublist(0, id), ...secondPartOfList];
              listKey.value += 1;
              break;
          }
        },
      );

      return () => eventBusSubscription.cancel();
    }, []);

    useEffect(() {
      note.value = AppState.getNoteById(id);
      isList.value = note.value.isList;

      if (isList.value) {
        list.value = contentToList(note.value.content);
      }

      return;
    }, [id]);

    Widget getTitleWidget() {
      if (editing.value) {
        return TextFormField(
          initialValue: note.value.title,
          style: titleTextStyle,
          onChanged: (v) => note.value.title = v,
          textInputAction: TextInputAction.next,
        );
      } else {
        return Text(
          note.value.title,
          style: titleTextStyle,
        );
      }
    }

    Widget getList() {
      return Column(
        children: [
          for (int i = 0; i < list.value.length; i++)
            ListElement(
              value: list.value[i],
              id: i,
              isNew: false,
              editable: false,
            ),
        ],
      );
    }

    Widget getEditingList() {
      return Column(
        key: Key("${listKey.value}"),
        children: [
          for (int i = 0; i < list.value.length; i++)
            ListElement(value: list.value[i], id: i, isNew: false),
          ListElement(value: "", id: list.value.length, isNew: true)
        ],
      );
    }

    Widget getContentWidget() {
      final content = note.value.content;
      if (editing.value) {
        if (isList.value) {
          return getEditingList();
        }
        return TextFormField(
          initialValue: content,
          keyboardType: TextInputType.multiline,
          maxLines: null,
          style: contentTextStyle,
          onChanged: (v) => note.value.content = v,
          //textInputAction: TextInputAction.send,
          //onFieldSubmitted: (_){
          //  EventBus.send(SaveClicked());
          //  AppState.saveNote(note.value);
          //},
        );
      } else {
        if (isList.value) {
          return getList();
        }

        return Text(
          content,
          style: contentTextStyle,
        );
      }
    }

    return Padding(
      padding: const EdgeInsets.only(
          left: _PAGE_PADDING_SIZE,
          right: _PAGE_PADDING_SIZE,
          top: _PAGE_PADDING_SIZE),
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    getTitleWidget(),
                    const SizedBox(
                      height: 16,
                    ),
                    if (editing.value)
                      Row(
                        children: [
                          const Text(
                            "List:",
                            style: contentTextStyle,
                          ),
                          Checkbox(
                              value: isList.value,
                              onChanged: (value) {
                                if (editing.value) {
                                  isList.value = value ?? false;
                                  note.value.isList = isList.value;
                                  list.value =
                                      contentToList(note.value.content);
                                }
                              }),
                        ],
                      ),
                    const SizedBox(
                      height: 16,
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: getContentWidget(),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: BOTTOM_BAR_HEIGHT),
            child: ButtonPanel(getButtons()),
          ),
        ],
      ),
    );
  }
}

const double _PAGE_PADDING_SIZE = 32;

class ListElement extends StatelessWidget {
  final String value;
  final int id;
  final bool isNew;
  final bool editable;

  const ListElement({
    super.key,
    required this.value,
    required this.id,
    required this.isNew,
    this.editable = true,
  });

  Widget getValueWidget() {
    if (editable) {
      return TextFormField(
        key: Key("$id"),
        initialValue: getTextWithoutSuffix(value),
        keyboardType: TextInputType.name,
        maxLines: 1,
        style: contentTextStyle,
        onChanged: (v) {
          if (isNew) {
            EventBus.send(ListElementAdded(v));
          } else {
            EventBus.send(ListElementChanged(v, id));
          }
        },
      );
    }
    return Text(
      getTextWithoutSuffix(value),
      style: contentTextStyle.merge(
        TextStyle(
          decoration: value.endsWith(_CROSSED_OUT_SUFFIX)
              ? TextDecoration.lineThrough
              : TextDecoration.none,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(
            value: value.endsWith(_CROSSED_OUT_SUFFIX),
            onChanged: (v) {
              if (v == true) {
                EventBus.send(
                    ListElementChanged(value + _CROSSED_OUT_SUFFIX, id));
              } else {
                EventBus.send(ListElementChanged(
                  getTextWithoutSuffix(value),
                  id,
                ));
              }
            }),
        Expanded(child: getValueWidget()),
        if (isNew == false && editable)
          IconButton(
            onPressed: () {
              EventBus.send(ListElementRemoved(id));
            },
            icon: const Icon(Icons.delete),
          ),
      ],
    );
  }
}

String listToContent(List<String> list) {
  return list.join("\n");
}

List<String> contentToList(String content) {
  return content.split("\n");
}

String getTextWithoutSuffix(String value) {
  if (value.endsWith(_CROSSED_OUT_SUFFIX)) {
    return value.substring(0, value.length - _CROSSED_OUT_SUFFIX.length);
  }
  return value;
}

String getDisplayableNoteContent(Note note) {
  if (note.isList) {
    var list = contentToList(note.content);
    var filteredList = list
        .where((element) => !element.endsWith(_CROSSED_OUT_SUFFIX))
        .toList();

    return listToContent(filteredList);
  }

  return note.content;
}

const _CROSSED_OUT_SUFFIX = "(---)";
