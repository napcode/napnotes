import 'dart:async' show StreamSubscription;
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:napnotes/core/app_state.dart';
import 'package:napnotes/styles.dart';
import 'package:napnotes/views/button_panel.dart';
import 'package:napnotes/widgets/bottom_app_bar.dart';
import 'package:napnotes/widgets/date_widget.dart';
import 'package:napnotes/widgets/time_widget.dart';
import 'package:napnotes/core/event_bus.dart';
import 'package:napnotes/core/events.dart';

class EventDetailView extends HookWidget {
  final String? id;

  EventDetailView({required this.id});

  @override
  Widget build(BuildContext context) {
    final event = useState(AppState.getEventById(id));
    final editing = useState(id == null);

    List<ButtonData> getButtons() {
      final buttons = <ButtonData>[];
      if (editing.value == false) {
        buttons.add(ButtonData('Edit', Icons.mode_edit, () {
          EventBus.send(EditClicked());
        }));
      } else {
        buttons.add(ButtonData('Save', Icons.save, () {
          EventBus.send(SaveClicked());
          AppState.saveEvent(event.value);
        }));
        buttons.add(ButtonData('Cancel', Icons.cancel, () {
          event.value = AppState.getEventById(id);
          EventBus.send(CancelClicked());
        }));
      }
      buttons.add(ButtonData('Delete', Icons.delete, () {
        showDeleteModal(context, () {
          AppState.deleteEvent(event.value);
          EventBus.send(DeleteClicked());
        });
      }));

      return buttons;
    }

    useEffect(() {
      StreamSubscription? eventBusSubscription = EventBus.stream.listen(
        (event) {
          switch (event.runtimeType) {
            case EditClicked:
              editing.value = true;
              break;
            case CancelClicked:
              editing.value = false;
              break;
            case SaveClicked:
              //TODO: AppState update note
              editing.value = false;
              break;
          }
        },
      );

      return () => eventBusSubscription.cancel();
    }, []);

    useEffect(() {
      event.value = AppState.getEventById(id);

      return;
    }, [id]);

    Widget getTitleWidget() {
      if (editing.value) {
        return TextFormField(
          initialValue: event.value.title,
          style: titleTextStyle,
          onChanged: (v) => event.value.title = v,
          textInputAction: TextInputAction.next,
        );
      } else {
        return Text(
          event.value.title,
          style: titleTextStyle,
        );
      }
    }

    Widget getDateWidget() {
      if (editing.value) {
        return DateWidget(
          date: event.value.date,
          onChanged: (newDate) {
            event.value.date = newDate.copyWith(
              hour: event.value.date.hour,
              minute: event.value.date.minute,
            );
          },
        );
      } else {
        return Text(
          event.value.formattedDate,
          style: contentTextStyle,
        );
      }
    }

    Widget getTimeWidget() {
      if (editing.value) {
        return TimeWidget(
          date: event.value.date,
          onChanged: (newDate) {
            event.value.date = event.value.date.copyWith(
              hour: newDate.hour,
              minute: newDate.minute,
            );
          },
        );
      } else {
        return Text(
          event.value.formattedTime,
          style: contentTextStyle,
        );
      }
    }

    Widget getContentWidget() {
      final content = event.value.content;
      if (editing.value) {
        return TextFormField(
          initialValue: content,
          keyboardType: TextInputType.multiline,
          maxLines: null,
          style: contentTextStyle,
          onChanged: (v) => event.value.content = v,
          // textInputAction: TextInputAction.send,
          // onFieldSubmitted: (_) {
          //   EventBus.send(SaveClicked());
          //   AppState.saveEvent(event.value);
          // },
        );
      } else {
        return Text(
          content,
          style: contentTextStyle,
        );
      }
    }

    return Padding(
      padding: const EdgeInsets.only(
        left: _PAGE_PADDING_SIZE,
        right: _PAGE_PADDING_SIZE,
        top: _PAGE_PADDING_SIZE,
      ),
      child: Column(
        children: [
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                getTitleWidget(),
                const SizedBox(
                  height: 32,
                ),
                getDateWidget(),
                const SizedBox(
                  height: 32,
                ),
                getTimeWidget(),
                const SizedBox(
                  height: 32,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: getContentWidget(),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: BOTTOM_BAR_HEIGHT),
            child: ButtonPanel(getButtons()),
          ),
        ],
      ),
    );
  }
}

const double _PAGE_PADDING_SIZE = 32;

void showDeleteModal(BuildContext context, VoidCallback onDelete) {
  showModalBottomSheet(
    context: context,
    builder: (BuildContext context) {
      return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text(
              'Deleting note',
              style: titleTextStyle,
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Are you sure you want to delete this note?\nThis action cannot be undone.',
              style: contentTextStyle,
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 20,
            ),
            ButtonPanel([
              ButtonData('Cancel', Icons.door_back_door_outlined, () {
                Navigator.pop(context);
              }),
              ButtonData('Delete', Icons.delete, () {
                onDelete();
                Navigator.pop(context);
              })
            ])
          ],
        ),
      );
    },
  );
}
