import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import 'package:napnotes/core/app_state.dart';
import 'package:napnotes/core/event_bus.dart';
import 'package:napnotes/core/events.dart';
import 'package:napnotes/widgets/bottom_app_bar.dart';
import 'package:napnotes/widgets/note_card.dart';

class NotesView extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final showOnlyFavorites = useState(false);
    final searchFiltered = useState('');
    useAppState(context);

    useEffect(() {
      StreamSubscription? eventBusSubscription =
          EventBus.stream.listen((event) {
        if (event.runtimeType == FilterFavoriteClicked) {
          showOnlyFavorites.value = !showOnlyFavorites.value;
        }
        if (event.runtimeType == SearchPhraseChanged) {
          searchFiltered.value = (event as SearchPhraseChanged).searchPhrase;
        }
      });

      return () => eventBusSubscription.cancel();
    }, []);

    return ListView(
        padding: const EdgeInsets.only(bottom: BOTTOM_BAR_HEIGHT_WITH_OPTIONS),
        scrollDirection: Axis.vertical,
        children: [
          for (var note in AppState.notes.where((n) {
            if (showOnlyFavorites.value) {
              return AppState.user.favorite.contains(n.id);
            }

            return true;
          }).where((n) {
            if (searchFiltered.value.isEmpty) {
              return true;
            }

            return n.title.toLowerCase().contains(searchFiltered.value) ||
                n.content.toLowerCase().contains(searchFiltered.value);
          }))
            NoteCard(note: note)
        ]);
  }
}
