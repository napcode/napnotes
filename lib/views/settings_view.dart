import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:napnotes/core/app_state.dart';
import 'package:napnotes/core/dataclasses.dart';
import 'package:napnotes/styles.dart';

class SettingsView extends HookWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        UserSelect(),
        IconButton(
          icon: const Icon(Icons.refresh),
          onPressed: () {
            AppState.refresh();
          },
        ),
      ],
    );
  }
}

class UserSelect extends HookWidget {
  final users = List<User>.from(AppState.users)..add(User.noname());

  @override
  Widget build(BuildContext context) {
    useAppState(context);
    return DropdownButton<String>(
      value: AppState.user.id,
      icon: const Icon(Icons.arrow_downward),
      elevation: 16,
      style: const TextStyle(color: secondaryColor),
      underline: Container(
        height: 2,
        color: secondaryColor,
      ),
      onChanged: (String? id) {
        if (id != null) {
          AppState.setUser(id);
        }
      },
      items: users.map<DropdownMenuItem<String>>((User user) {
        return DropdownMenuItem<String>(
          value: user.id,
          child: Text(user.name),
        );
      }).toList(),
    );
  }
}
