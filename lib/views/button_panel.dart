import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:napnotes/styles.dart';

class ButtonPanel extends HookWidget {
  final List<ButtonData> data;

  const ButtonPanel(this.data);

  Widget getButton(int i) {
    final btn = data[i];
    return GestureDetector(
      onTap: () {
        btn.onClick();
      },
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              btn.iconData,
              color: primaryColor,
            ),
            Text(
              btn.text,
              style: buttonTextStyle,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(color: primaryColor, width: 3),
            borderRadius: const BorderRadius.all(Radius.circular(50))),
        child: Wrap(
          children: [
            for (int i = 0; i < data.length; i++) getButton(i),
          ],
        ),
      ),
    );
  }
}

class ButtonData {
  final String text;
  final IconData iconData;
  final VoidCallback onClick;

  ButtonData(this.text, this.iconData, this.onClick);
}

void showDeleteModal(BuildContext context, VoidCallback onDelete) {
  showModalBottomSheet(
    context: context,
    builder: (BuildContext context) {
      return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text(
              'Deleting note',
              style: titleTextStyle,
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Are you sure you want to delete this note?\nThis action cannot be undone.',
              style: contentTextStyle,
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 20,
            ),
            ButtonPanel([
              ButtonData('Cancel', Icons.door_back_door_outlined, () {
                Navigator.pop(context);
              }),
              ButtonData('Delete', Icons.delete, () {
                onDelete();
                Navigator.pop(context);
              })
            ])
          ],
        ),
      );
    },
  );
}
