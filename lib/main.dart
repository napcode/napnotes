import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:napnotes/core/enums.dart';

import 'package:napnotes/core/env.dart';
import 'package:napnotes/core/api.dart';
import 'package:napnotes/core/event_bus.dart';
import 'package:napnotes/core/app_state.dart';
import 'package:napnotes/core/events.dart';
import 'package:napnotes/styles.dart';
import 'package:napnotes/widgets/bottom_app_bar.dart';
import 'package:napnotes/widgets/loading_screen.dart';
import 'package:napnotes/widgets/router.dart';

void main() async {
  if (kDebugMode) {
    EventBus.stream.listen((event) => print(event));
  }

  runApp(const MyApp());

  await AppState.load();
  await Env.load();
  Api.object = Api(url: Env.url, db: Env.db, token: Env.token);
  await AppState.update();
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends HookWidget {
  MyHomePage({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    final lifecycle = useAppLifecycleState();

    useEffect(() {
      if (lifecycle == AppLifecycleState.resumed && Api.object != null) {
        AppState.update();
      }
      return;
    }, [lifecycle]);
    useEffect(() {
      StreamSubscription? eventBusSubscription =
          EventBus.stream.listen((event) {
        if (_FAILED_EVENTS.contains(event.runtimeType)) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text("Upss... something went wrong!"),
              backgroundColor: errorColor,
            ),
          );
        }
      });

      return () => eventBusSubscription.cancel();
    }, []);

    return WillPopScope(
      onWillPop: () async {
        if (AppState.currentView == AppView.events) {
          return true;
        }

        EventBus.send(BackButtonClicked());
        return false;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: Stack(
            children: [
              const MyRouter(),
              Align(
                alignment: Alignment.bottomCenter,
                child: MyBottomAppBar(),
              ),
              const LoadingScreen()
            ],
          ),
        ),
      ),
    );
  }
}

const _FAILED_EVENTS = [
  NoteSaveFailed,
  NoteDeleteFailed,
  EventSaveFailed,
  EventDeleteFailed
];
