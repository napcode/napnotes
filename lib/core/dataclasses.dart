import 'package:uuid/uuid.dart';
import 'package:intl/intl.dart';
import 'package:napnotes/core/utils.dart';

class Note {
  String id;
  String title;
  String content;
  String owner;
  int created;
  int updated;
  List<String> tags;
  bool isList;

  Note({
    required this.id,
    required this.title,
    required this.content,
    required this.owner,
    this.created = 0,
    this.updated = 0,
    this.tags = const <String>[],
    this.isList = false,
  });

  Note.fromJson(Map<String, dynamic> data)
      : id = data['id'] ?? '',
        title = data['title'] ?? '',
        content = data['content'] ?? '',
        owner = data['owner'] ?? '',
        created = data['created'] ?? 0,
        updated = data['updated'] ?? 0,
        tags = <String>[...data['tags'] ?? []],
        isList = data['isList'] ?? false;

  Note.empty()
      : id = Uuid().v4(),
        title = '',
        content = '',
        owner = '',
        created = 0,
        updated = 0,
        tags = const <String>[],
        isList = false;

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'content': content,
        'onwer': owner,
        'created': created,
        'updated': updated,
        'tags': tags,
        'isList': isList
      };
}

class CalendarEvent {
  String id;
  String title;
  String content;
  String owner;
  DateTime date;
  int created = 0;
  int updated = 0;
  List<String> tags = <String>[];

  CalendarEvent({
    required this.id,
    required this.title,
    required this.content,
    required this.owner,
    required this.date,
    this.created = 0,
    this.updated = 0,
    this.tags = const <String>[],
  });

  CalendarEvent.fromJson(Map<String, dynamic> data)
      : id = data['id'] ?? '',
        title = data['title'] ?? '',
        content = data['content'] ?? '',
        owner = data['owner'] ?? '',
        date = toDateTime(data['date']) ?? DateTime.now(),
        created = data['created'] ?? 0,
        updated = data['updated'] ?? 0,
        tags = <String>[...data['tags'] ?? []];

  CalendarEvent.empty()
      : id = Uuid().v4(),
        title = '',
        content = '',
        owner = '',
        date = DateTime.now(),
        created = 0,
        updated = 0,
        tags = const <String>[];

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'content': content,
        'owner': owner,
        'date': toTimestamp(date),
        'created': created,
        'updated': updated,
        'tags': tags,
      };

  String get formattedTime {
    return DateFormat('Hm').format(date);
  }

  String get formattedDate {
    return DateFormat('dd/MM/yyy').format(date);
  }
}

class User {
  String id;
  String name;
  int updated;
  List<String> favorite;

  User({
    required this.id,
    required this.name,
    required this.favorite,
    this.updated = 0,
  });

  User.noname()
      : id = '',
        name = 'noname',
        updated = 0,
        favorite = const <String>[];

  User.fromJson(Map<String, dynamic> data)
      : id = data['id']!,
        name = data['name'] ?? 'noname',
        updated = data['updated'] ?? 0,
        favorite = List<String>.from(data['favorite']!);

  Map<String, dynamic> toJson() =>
      {'id': id, 'name': name, 'favorite': favorite, 'updated': updated};
}

class Day {
  DateTime date;
  List<CalendarEvent> events;

  Day({required this.date, List<CalendarEvent>? events})
      : events = events ?? [];

  String get formattedDate {
    return DateFormat('EEEE, dd-MM-yyyy').format(date);
  }

  int get daysSinceToday {
    final today = DateTime.now();
    return daysBetween(today, date);
  }

  String get daysSinceTodayString {
    return 'IN $daysSinceToday DAYS';
  }
}
