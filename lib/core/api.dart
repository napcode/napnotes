import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart' show kDebugMode;

import 'package:napnotes/core/dataclasses.dart';

class ApiException implements Exception {
  int statusCode;
  String body;
  ApiException(this.statusCode, this.body);
}

class Api {
  String url;
  String db;
  String token;

  static Api? object;

  Api({required this.url, required this.db, required this.token});

  static Api get obj {
    if (object == null) {
      throw Exception('Api object is not initialized');
    }

    return object!;
  }

  Future<http.Response> post(Map<String, dynamic> body) {
    return http
        .post(
          Uri.parse('$url/storage/cmd'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': 'Token $token'
          },
          body: jsonEncode(body),
        )
        .timeout(const Duration(seconds: 5));
  }

  Future<Map<String, dynamic>> send(Map<String, dynamic> body) async {
    final response = await post(body);

    if (response.statusCode != 200) {
      if (kDebugMode) {
        print("${response.statusCode}: ${response.body}");
      }
      throw ApiException(response.statusCode, response.body);
    }
    final responseBody = utf8.decode(response.bodyBytes);

    return jsonDecode(responseBody);
  }

  Future<Map<String, dynamic>> getAll() async {
    var body = {
      'key': db,
      'type': 'get',
      'fields': ['notes', 'events', 'users']
    };
    return await send(body);
  }

  Future<Map<String, dynamic>> getUpdates(int timestamp) async {
    var where = {
      'field': 'updated',
      'operator': '>=',
      'value': timestamp,
      'type': 'int',
    };
    var body = {
      'key': db,
      'type': 'get',
      'fields': [
        {'field': 'notes', 'where': where},
        {'field': 'events', 'where': where},
        {'field': 'users', 'where': where},
        {'field': 'deleted_notes', 'where': where},
        {'field': 'deleted_events', 'where': where},
      ]
    };
    return await send(body);
  }

  Future<void> addNote(Note note) async {
    var body = {
      'key': db,
      'type': 'add',
      'field': 'notes',
      'value': note.toJson()
    };
    await send(body);
  }

  Future<void> updateNote(Note note) async {
    var body = {
      'key': db,
      'type': 'set',
      'field': 'notes',
      'value': note.toJson(),
      'where': {
        'field': 'id',
        'value': note.id,
      }
    };
    await send(body);
  }

  Future<void> deleteNote(Note note, int timestamp) async {
    var body = {
      'key': db,
      'commands': [
        {
          'type': 'remove',
          'field': 'notes',
          'where': {
            'field': 'id',
            'value': note.id,
          }
        },
        {
          'type': 'add',
          'field': 'deleted_notes',
          'value': {
            'id': note.id,
            'updated': timestamp,
          },
          'max_length': 32
        },
      ]
    };
    await send(body);
  }

  Future<void> addEvent(CalendarEvent event) async {
    var body = {
      'key': db,
      'type': 'add',
      'field': 'events',
      'value': event.toJson()
    };
    await send(body);
  }

  Future<void> updateEvent(CalendarEvent event) async {
    var body = {
      'key': db,
      'type': 'set',
      'field': 'events',
      'value': event.toJson(),
      'where': {
        'field': 'id',
        'value': event.id,
      }
    };
    await send(body);
  }

  Future<void> deleteEvent(CalendarEvent event, int timestamp) async {
    var body = {
      'key': db,
      'commands': [
        {
          'type': 'remove',
          'field': 'events',
          'where': {
            'field': 'id',
            'value': event.id,
          }
        },
        {
          'type': 'add',
          'field': 'deleted_events',
          'value': {
            'id': event.id,
            'updated': timestamp,
          },
          'max_length': 32
        },
      ]
    };
    await send(body);
  }
}
