import 'dart:io';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';

class Storage {
  static const fileName = 'napnotes.json';

  static save(Map<String, dynamic> data) async {
    final file = await _file;
    final jsonString = json.encode(data);
    await file.writeAsString(jsonString);
  }

  static Future<Map<String, dynamic>> read() async {
    final file = await _file;
    final exists = await file.exists();

    if (!exists) {
      file.writeAsString('{}');
    }

    final jsonString = await file.readAsString();

    return json.decode(jsonString);
  }

  static Future<String> get _filePath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  static Future<File> get _file async {
    final path = await _filePath;
    return File('$path/data.json');
  }
}
