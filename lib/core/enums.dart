enum AppView {
  notes,
  events,
  settings,
  noteDetail,
  addNote,
  eventDetail,
  addEvent,
}
