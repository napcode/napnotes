import 'package:napnotes/core/event_bus.dart';
import 'package:napnotes/core/enums.dart';

class ApiResponseReceived with Event {
  final String name;
  final Map<String, dynamic> data;
  ApiResponseReceived({required this.name, required this.data});

  @override
  String toString() {
    return '$runtimeType: $name, $data';
  }
}

class ApiErrorOccurred with Event {
  final String name;
  final int status;
  final String body;

  ApiErrorOccurred(
      {required this.name, required this.status, required this.body});

  @override
  String toString() {
    return '$runtimeType: $name, $status, $body';
  }
}

class BottomAppBarButtonPressed with Event {
  final AppView view;

  BottomAppBarButtonPressed({required this.view});

  @override
  String toString() {
    return '$runtimeType: $view';
  }
}

class RouteChanged with Event {
  final AppView view;

  RouteChanged({required this.view});

  @override
  String toString() {
    return '$runtimeType: $view';
  }
}

class BackButtonClicked with Event {}

class NoteClicked with Event {
  final String? id;

  NoteClicked(this.id);

  @override
  String toString() {
    return '$runtimeType: $id';
  }
}

class EditClicked with Event {}

class CancelClicked with Event {}

class SaveClicked with Event {}

class NoteSaved with Event {
  final String? id;

  NoteSaved(this.id);

  @override
  String toString() {
    return '$runtimeType: $id';
  }
}

class NoteSaveFailed with Event {}

class DeleteClicked with Event {}

class NoteDeleted with Event {}

class NoteDeleteFailed with Event {}

class EventClicked with Event {
  final String? id;

  EventClicked(this.id);

  @override
  String toString() {
    return '$runtimeType: $id';
  }
}

class EventSaved with Event {
  final String? id;

  EventSaved(this.id);

  @override
  String toString() {
    return '$runtimeType: $id';
  }
}

class EventSaveFailed with Event {}

class EventDeleted with Event {}

class EventDeleteFailed with Event {}

class FilterFavoriteClicked with Event {}

class SearchPhraseChanged with Event {
  final String searchPhrase;

  SearchPhraseChanged(this.searchPhrase);

  @override
  String toString() {
    return '$runtimeType: $searchPhrase';
  }
}

class ShowAllEventsClicked with Event {}

class ApiUpdateFailed with Event {}

class ApiRefreshFailed with Event {}

class ListElementChanged with Event {
  final String value;
  final int id;

  ListElementChanged(this.value, this.id);

  @override
  String toString() {
    return '$runtimeType: $value $id';
  }
}

class ListElementAdded with Event {
  final String value;

  ListElementAdded(this.value);

  @override
  String toString() {
    return '$runtimeType: $value';
  }
}

class ListElementRemoved with Event {
  final int id;

  ListElementRemoved(this.id);

  @override
  String toString() {
    return '$runtimeType: $id';
  }
}

