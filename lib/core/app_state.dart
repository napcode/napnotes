import 'dart:async';

import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import 'package:napnotes/core/api.dart';
import 'package:napnotes/core/storage.dart';
import 'package:napnotes/core/dataclasses.dart';
import 'package:napnotes/core/events.dart';
import 'package:napnotes/core/event_bus.dart';
import 'package:napnotes/core/enums.dart';
import 'package:napnotes/core/utils.dart';

class AppState {
  static List<Note> notes = [];
  static List<CalendarEvent> events = [];
  static List<User> users = [];

  // Version is a local variable to track changes and update views with hook
  static ValueNotifier<int> version = ValueNotifier<int>(0);
  static AppView currentView = AppView.events;
  static int lastUpdateTimestamp = 0;
  static User user = User.noname();

  static incrementVersion() {
    version.value++;
  }

  static Future<void> refresh() async {
    try {
      final data = await Api.obj.getAll();
      parseLists(data);
      await save();
    } catch (e) {
      EventBus.send(ApiRefreshFailed());
      if (kDebugMode) {
        rethrow;
      }
    }
  }

  static Future<void> update() async {
    try {
      final data = await Api.obj.getUpdates(lastUpdateTimestamp);

      final updatedNotes = data['notes'] ?? [];
      for (final noteJson in updatedNotes) {
        final note = Note.fromJson(noteJson);
        final index = notes.indexWhere((n) => n.id == note.id);
        if (index == -1) {
          notes.add(note);
        } else {
          notes[index] = note;
        }
      }

      var updatedEvents = data['events'] ?? [];
      for (final eventJson in updatedEvents) {
        final event = CalendarEvent.fromJson(eventJson);
        final index = events.indexWhere((e) => e.id == event.id);
        if (index == -1) {
          events.add(event);
        } else {
          events[index] = event;
        }
      }

      var updatedUsers = data['users'] ?? [];
      for (final userJson in updatedUsers) {
        final user = User.fromJson(userJson);
        final index = users.indexWhere((u) => u.id == user.id);
        if (index == -1) {
          users.add(user);
        } else {
          users[index] = user;
        }
      }

      var deletedNotes = data['deleted_notes'] ?? [];
      for (final deleted in deletedNotes) {
        notes.removeWhere((n) => n.id == deleted['id']);
      }

      var deletedEvents = data['deleted_events'] ?? [];
      for (final deleted in deletedEvents) {
        events.removeWhere((e) => e.id == deleted['id']);
      }

      lastUpdateTimestamp = toTimestamp(DateTime.now());
      await save();
    } catch (e) {
      EventBus.send(ApiUpdateFailed());
      if (kDebugMode) {
        rethrow;
      }
    }
  }

  static Future<void> load() async {
    try {
      final data = await Storage.read();
      parseLists(data);
      lastUpdateTimestamp = data['timestamp'] ?? 0;
      user = data['user'] != null ? User.fromJson(data['user']) : User.noname();
      incrementVersion();
    } catch (e) {
      if (kDebugMode) {
        rethrow;
      }
    }
  }

  static Future<void> save() async {
    try {
      sortLists();
      final data = serialize();
      await Storage.save(data);
    } catch (e) {
      if (kDebugMode) {
        rethrow;
      }
    }
    incrementVersion();
  }

  static void parseLists(Map<String, dynamic> data) {
    if (data.containsKey('notes')) {
      final list = <Note>[];
      for (final note in data['notes']) {
        list.add(Note.fromJson(note));
      }
      notes = list;
    }
    if (data.containsKey('events')) {
      final list = <CalendarEvent>[];
      for (final event in data['events']) {
        list.add(CalendarEvent.fromJson(event));
      }
      events = list;
    }
    if (data.containsKey('users')) {
      final list = <User>[];
      for (final user in data['users']) {
        list.add(User.fromJson(user));
      }
      users = list;
    }
  }

  static Map<String, dynamic> serialize() {
    final data = <String, dynamic>{};
    data['notes'] = notes.map((n) => n.toJson()).toList();
    data['events'] = events.map((e) => e.toJson()).toList();
    data['users'] = users.map((u) => u.toJson()).toList();
    data['timestamp'] = lastUpdateTimestamp;
    data['user'] = user.toJson();

    return data;
  }

  static void sortLists() {
    notes.sort((a, b) => b.updated.compareTo(a.updated));
    events.sort((a, b) => a.date.compareTo(b.date));
  }

  static Note getNoteById(String? id) {
    final index = notes.indexWhere((n) => n.id == id);
    if (index == -1) {
      return Note.empty();
    }

    return Note.fromJson(notes[index].toJson());
  }

  static Future<void> saveNote(Note note) async {
    note.updated = getNowTimestamp();
    final index = notes.indexWhere((n) => n.id == note.id);
    if (index != -1) {
      try {
        await Api.obj.updateNote(note);
      } catch (e) {
        EventBus.send(NoteSaveFailed());
        if (kDebugMode) {
          rethrow;
        }
        return;
      }
      notes[index] = note;
    } else {
      note.created = note.updated;
      note.owner = user.id;
      try {
        await Api.obj.addNote(note);
      } catch (e) {
        EventBus.send(NoteSaveFailed());
        if (kDebugMode) {
          rethrow;
        }
        return;
      }
      notes.insert(0, note);
    }
    save();
    incrementVersion();
    EventBus.send(NoteSaved(note.id));
  }

  static void deleteNote(Note note) async {
    final timestamp = getNowTimestamp();
    try {
      await Api.obj.deleteNote(note, timestamp);
    } catch (e) {
      EventBus.send(NoteDeleteFailed());
      if (kDebugMode) {
        rethrow;
      }
      return;
    }
    notes.removeWhere((n) => n.id == note.id);
    await save();
    EventBus.send(NoteDeleted());
  }

  static CalendarEvent getEventById(String? id) {
    final index = events.indexWhere((n) => n.id == id);
    if (index == -1) {
      return CalendarEvent.empty();
    }

    return CalendarEvent.fromJson(events[index].toJson());
  }

  static Future<void> saveEvent(CalendarEvent event) async {
    event.updated = getNowTimestamp();
    final index = events.indexWhere((n) => n.id == event.id);
    if (index != -1) {
      try {
        await Api.obj.updateEvent(event);
      } catch (e) {
        EventBus.send(EventSaveFailed());
        if (kDebugMode) {
          rethrow;
        }
        return;
      }
      events[index] = event;
    } else {
      event.created = event.updated;
      event.owner = user.id;
      try {
        await Api.obj.addEvent(event);
      } catch (e) {
        EventBus.send(EventSaveFailed());
        if (kDebugMode) {
          rethrow;
        }
        return;
      }
      events.insert(0, event);
    }
    await save();
    EventBus.send(EventSaved(event.id));
  }

  static void deleteEvent(CalendarEvent event) async {
    final timestamp = getNowTimestamp();
    try {
      await Api.obj.deleteEvent(event, timestamp);
    } catch (e) {
      EventBus.send(EventDeleteFailed());
      if (kDebugMode) {
        rethrow;
      }
      return;
    }
    events.removeWhere((n) => n.id == event.id);
    save();
    incrementVersion();
    EventBus.send(EventDeleted());
  }

  static void setCurrentView(AppView view) {
    currentView = view;
    incrementVersion();
    EventBus.send(RouteChanged(view: view));
  }

  static void setUser(String id) {
    user = users.firstWhere((u) => u.id == id, orElse: () => User.noname());
    save();
  }
}

void useAppState(BuildContext context) {
  final version = useState<int>(AppState.version.value);

  void incrementVersion() {
    version.value++;
  }

  useEffect(() {
    AppState.version.addListener(incrementVersion);
    return () {
      AppState.version.removeListener(incrementVersion);
      return;
    };
  }, []);
}
