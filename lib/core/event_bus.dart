import 'dart:async';

class Event {
  @override
  String toString() {
    return '$runtimeType';
  }
}

class EventBus {
  static var controller = StreamController<Event>.broadcast();
  static Stream get stream {
    return EventBus.controller.stream;
  }

  static Sink get sink {
    return EventBus.controller.sink;
  }

  static void send(Event event) {
    EventBus.sink.add(event);
  }
}
