int toTimestamp(DateTime date) {
  return (date.millisecondsSinceEpoch / 1000).floor();
}

DateTime toDateTime(int date) {
  return DateTime.fromMillisecondsSinceEpoch(date * 1000);
}

int getTodayTimestamp() {
  final today = getDay(DateTime.now());

  return toTimestamp(today);
}

DateTime getDay(DateTime date) {
  return DateTime(date.year, date.month, date.day);
}

int getNowTimestamp() {
  DateTime now = DateTime.now();

  return toTimestamp(now);
}

int daysBetween(DateTime from, DateTime to) {
  from = getDay(from);
  to = getDay(to);
  return (to.difference(from).inHours / 24).round();
}
