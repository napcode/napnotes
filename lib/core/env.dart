import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class Env {
  static const String path = String.fromEnvironment('env');

  static late final String url;
  static late final String db;
  static late final String token;

  static Future<void> load() async {
    var jsonString = await rootBundle.loadString('.env.json');
    var jsonData = json.decode(jsonString);

    url = jsonData['url'];
    db = jsonData['db'];
    token = jsonData['token'];
  }
}
